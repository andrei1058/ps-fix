package com.andrei1058.psfix;

import net.sacredlabyrinth.Phaed.PreciousStones.PreciousStones;
import net.sacredlabyrinth.Phaed.PreciousStones.field.FieldFlag;
import org.bukkit.Bukkit;
import org.bukkit.entity.Trident;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

public class PSFix extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        Plugin p = Bukkit.getPluginManager().getPlugin("PreciousStones");

        if (p == null) {
            getLogger().log(Level.WARNING, "Could not enable because PreciousStones is missing!");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        try {
            Class.forName("org.bukkit.entity.Trident");
        } catch (ClassNotFoundException e) {
            getLogger().log(Level.WARNING, "Your server version is not supported! Use 1.13 or newer.");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        if (!p.getDescription().getMain().equals("net.sacredlabyrinth.Phaed.PreciousStones.PreciousStones")) {
            getLogger().log(Level.WARNING, "You are not using the right PreciousStones plugin!");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onProjectile(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Trident) {
            if (PreciousStones.API().isFieldProtectingArea(FieldFlag.PREVENT_PVP, e.getEntity().getLocation())) {
                e.setCancelled(true);
            }
        }
    }
}
