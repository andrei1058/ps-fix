![img-header](./img-header.png)

This is a [PreciousStones](http://jenkins.elmakers.com/job/PreciousStones/) (by phaed) fix for 1.13 or newer servers. This will fix the issue where Tridents can be used to do pvp on protection fields.